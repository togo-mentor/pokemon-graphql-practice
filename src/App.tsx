import React, { useState } from "react";
import { ApolloProvider } from "@apollo/client";
import { apolloClient } from "./graphql/client";
import { SearchResultField } from "./components/SearchResultField";
import { SearchForm } from "./components/SearchForm";

export const App = () => {
  const [pokemonName, setpokemonName] = useState<string>("");
  return (
    <>
      <SearchForm setpokemonName={setpokemonName} />
      <ApolloProvider client={apolloClient}>
        <SearchResultField pokemonName={pokemonName} />
      </ApolloProvider>
    </>
  );
};
